## Recipe File Structure
The configuration below creates a recipe attached to a given bundle, with instructions to add a file to the application's 
config directory.

After the recipe is added a new entry will need to be made in this repository's index.json file. The given version should 
match or precede the version of the bundle being installed.

```
{
    "manifests": {
        "insyteful/[ RECIPE NAME ]": {
            "manifest": {
                "bundles": {
                    "Insyteful\\[ BUNDLE NAME ]\\Insyteful[ BUNDLE NAME ]": [
                        "all"
                    ]
                },
                "copy-from-recipe": {
                    "config/": "%CONFIG_DIR%"
                }
            },
            "files": {
                "[PATH TO COPY]": {
                    "contents": [
                        [ CONTENT TO BE COPIED ]
                    ],
                    "executable": false
                }
            },
            "ref": "[ bin2hex(random_bytes(20)) HASH ]"
        }
    }
}
```